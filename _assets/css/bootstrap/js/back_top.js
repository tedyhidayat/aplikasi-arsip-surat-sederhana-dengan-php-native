$(function(){
    $('.back_top').on('click', function(){
        $('html').animate({scrollTop : 0}, 800);
    });
    $(window).on('scroll', function(){
        if($(this).scrollTop() > 100){
            $('.back_top').show();
        }
    });
});