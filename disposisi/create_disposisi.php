<div class="row">
	<div class="col-sm-12">
		<h3></h3><br>
		<ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
		  <li><a href="?page=disposisi"><i class="fa fa-reply"></i> Data disposisi</a></li>
		  <li class="active">Buat disposisi</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1 canvas">
		<div class="col-sm-12">
			<div class="panel panel-success">
			  	<div class="panel-heading" style="background-color: #0099FF; color: white;">
			    	<h3 class="panel-title"><span class="fa fa-pencil"> Buat disposisi</span></h3>
			  	</div>

					<?php
					  $id = @$_GET['no_agenda'];
					  $sql = mysqli_query($conn, "SELECT * FROM tb_surat_masuk where no_agenda='$id'") or die (mysqli_error());
					  $data_dispo = mysqli_fetch_array($sql);

					  $carikode = mysqli_query($conn, "SELECT MAX(no_disposisi) from tb_disposisi") or die (mysqli_error($conn));
					  $datakode = mysqli_fetch_array($carikode);
					  if($datakode){
					    $nilaikode = substr($datakode[0], 7);
					    $kode = (int) $nilaikode;
					    $kode = $kode + 1;
					    $nodispo = "DSP-SM/".str_pad($kode, 4, "0", STR_PAD_LEFT);
					  }
					  else{
					    $nodispo = "SRT-K/0001";
					  }
					?>

			  	<form method="post" role="form" action="" class="form-horizontal" enctype="multipart/form-data">
				<div class="panel-body">
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="nodispo">No Disposisi</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="no_dispo" class="form-control" id="nodispo" value="<?= $nodispo; ?>" readonly>
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="noagenda">No Agenda</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="no_agenda" class="form-control" id="noagenda" value="<?= $data_dispo['no_agenda']; ?>" readonly>
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="nosurat">No surat</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="no_surat" class="form-control" id="nosurat" value="<?= $data_dispo['no_surat']; ?>" readonly>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="pengirim">Kepada</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="kepada" class="form-control" id="pengirim" placeholder="Kepada">
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="Perihal">Keterangan</label>
				    	  	<div class="col-sm-10 ">
				    	  		<textarea type="text" name="keterangan" rows="6" class="form-control" id="Perihal" placeholder="Keternagan"></textarea>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="tanggapan">Status surat</label>
				    		<div class="col-sm-10">
					    	  <select name="stat" class="form-control" id="tanggapan">
					    	    <option>-- Pilih status --</option>
					    	    <option value="Penting">Penting</option>
					    	    <option value="Sangat penting">Sangat penting</option>
					    	    <option value="Rahasia">Rahasia</option>
					    	    <option value="Umum">Umum</option>
					    	  </select>
					      	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="tanggapan">Tanggapan</label>
				    		<div class="col-sm-10">
					    	  <select name="tanggapan" class="form-control" id="tanggapan">
					    	    <option>-- Pilih tanggapan --</option>
					    	    <option value="Teruskan">Teruskan</option>
					    	    <option value="Tidak diteruskan">Tidak diteruskan</option>
					    	  </select>
					      	</div>
				    	</div>
				    	<input type="hidden" name="status" class="form-control" value="<?php echo'S'; ?>">
				</div>
			  	<div class="panel-footer text-right" style="background-color: #fff;">
			  		<input type="submit" name="tambah" class="btn btn-info" value="Disposisikan">
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  	</form>

			  	<?php
			  		$no_dispo 	= htmlspecialchars(@$_POST['no_dispo']);
			  		$no_agenda 	= htmlspecialchars(@$_POST['no_agenda']);
			  		$no_surat	= htmlspecialchars(@$_POST['no_surat']);
			  		$kepada 	= htmlspecialchars(@$_POST['kepada']);
			  		$keterangan = htmlspecialchars(@$_POST['keterangan']);
			  		$stat 		= htmlspecialchars(@$_POST['stat']);
			  		$tanggapan	= htmlspecialchars(@$_POST['tanggapan']);
			  		$status		= htmlspecialchars(@$_POST['status']);

			  		$tambah_data  = @$_POST['tambah'];

			  		if($tambah_data){
			  		    if($no_dispo == "" || $no_agenda == "" || $no_surat == "" || $kepada == "" || $keterangan == "" || $stat == "" || $tanggapan == ""){
			  		        ?>
			  		            <script type="text/javascript">
			  		                alert("Data tidak boleh kosong !");
			  		                window.location.href ="?page=disposisi&action=create_disposisi&no_agenda=<?= $data['no_agenda']; ?>";
			  		            </script>
			  		        <?php
			  		    }
			  		    else{
			  		    	//untuk tambah dispo
			  		    	$insert = "INSERT INTO tb_disposisi values('$no_dispo', '$no_agenda', '$no_surat', '$kepada', '$keterangan', '$stat', '$tanggapan')" or die (mysqli_error());
			  		    	//untuk update status surat sudah didisposisikaN
			  		    	$update = "UPDATE tb_surat_masuk SET status='$status' WHERE no_agenda='$no_agenda'" or die (mysqli_error());

			  		    	$sql1 = mysqli_query($conn, $insert);
			  		    	$sql2 = mysqli_query($conn, $update);

			  		    	$hasil = $sql1 . $sql2;

			  		    	if ($sql1) {
			  		    		?>
			  		    			<script type="text/javascript">
			  		    				alert("Disposisi berhasil dibuat !");
			  		    				window.location.href="?page=disposisi";
			  		    			</script>
			  		    		<?php
			  		    	} else {
			  		    		?>
			  		    			<script type="text/javascript">
			  		    				alert("Disposisi gagal dibuat !");
			  		    				window.location.href="?page=disposisi&action=create_disposisi&no_agenda=<?= $data['no_agenda']; ?>";
			  		    			</script>
			  		    		<?php
			  		    	}
			  		    }
			  		}//end tambahdata
			  	?>

			</div>
		</div>
	</div>
</div>