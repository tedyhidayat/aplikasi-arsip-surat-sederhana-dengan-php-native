<div class="row" style="padding-top: 35px; padding-bottom: 30px;">
	<div class="col-sm-12">
		<div class="well dashboard" style="box-shadow: 0px 0px 3px gray;">
			<span><b> <span class="fa fa-dashboard"></span> Dashboard</b></span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12"></div>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2">
		  <div class="col-sm-3 data-index">
		    <div class="thumbnail">
		      <img src="_assets/images/1.png" alt="..." width="100px" class="padding-icon">
		      <div class="caption">
		        <h3 class="text-center thumbnail-text">Data Petugas</h3>
		        <hr>
		        <h5 class="text-center">
		        	Jumlah
		        	<span class="label label-success">
		        		<?php
		        			$sql = "SELECT * FROM tb_petugas ORDER BY id DESC";

		        			if ($total = mysqli_query($conn, $sql)) {
		        				$row = mysqli_num_rows($total);
		        				echo $row;
		        			}
		        		?>
		        	</span>
		        </h5>
		        <hr>
		        <p class="text-center"><a href="?page=petugas" class="btn btn-primary" role="button"><span class="fa fa-tasks"></span> Lihat</a> <!-- <a href="#" class="btn btn-info" role="button"><span class="fa fa-print"></span> Print</a> --></p>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-3 data-index">
		    <div class="thumbnail">
		      <img src="_assets/images/mail.png" alt="..." width="100px" class="padding-icon">
		      <div class="caption">
		        <h3 class="text-center thumbnail-text">Data Surat Masuk</h3>
		        <hr>
		        <h5 class="text-center">
		        	Jumlah
		        	<span class="label label-success">
		        		<?php
		        			$sql = "SELECT * FROM tb_surat_masuk ORDER BY id DESC";

		        			if ($total = mysqli_query($conn, $sql)) {
		        				$row = mysqli_num_rows($total);
		        				echo $row;
		        			}
		        		?>
		        	</span>
		        </h5>
		        <hr>
		        <p class="text-center"><a href="?page=s_masuk" class="btn btn-primary" role="button"><span class="fa fa-tasks"></span> Lihat</a> <!-- <a href="#" class="btn btn-info" role="button"><span class="fa fa-print"> Print</a> --></p>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-3 data-index">
		    <div class="thumbnail">
		      <img src="_assets/images/open-mail.png" alt="..." width="100px" class="padding-icon">
		      <div class="caption">
		        <h3 class="text-center thumbnail-text">Data Surat Keluar</h3>
		        <hr>
		        <h5 class="text-center">
		        	Jumlah
		        	<span class="label label-success">
		        		<?php
		        			$sql = "SELECT * FROM tb_surat_keluar ORDER BY id DESC";

		        			if ($total = mysqli_query($conn, $sql)) {
		        				$row = mysqli_num_rows($total);
		        				echo $row;
		        			}
		        		?>
		        	</span>
		        </h5>
		        <hr>
		        <p class="text-center"><a href="?page=s_keluar" class="btn btn-primary" role="button"><span class="fa fa-tasks"></span> Lihat</a> <!-- <a href="#" class="btn btn-info" role="button"><span class="fa fa-print"> Print</a> --></p>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-3 data-index">
		    <div class="thumbnail">
		      <img src="_assets/images/lock.png" alt="..." width="100px" class="padding-icon">
		      <div class="caption">
		        <h3 class="text-center thumbnail-text">Disposisi Surat Masuk</h3>
		        <hr>
		        <h5 class="text-center">
		        	Jumlah
		        	<span class="label label-success">
		        		<?php
		        			$sql = "SELECT * FROM tb_disposisi ORDER BY no_disposisi DESC";

		        			if ($total = mysqli_query($conn, $sql)) {
		        				$row = mysqli_num_rows($total);
		        				echo $row;
		        			}
		        		?>
		        	</span>
		        </h5>
		        <hr>
		        <p class="text-center"><a href="#" class="btn btn-primary" role="button"><span class="fa fa-tasks"></span> Lihat</a> <!-- <a href="#" class="btn btn-info" role="button"><span class="fa fa-print"> Print</a> --></p>
		      </div>
		    </div>
		  </div>
	</div>
</div>