<div class="row">
    <div class="col-sm-12">
        <h3></h3><br>
        <ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
          <li class="active"><i class="fa fa-envelope"></i> Data Surat Masuk</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
            <div class="dropdowns">
                <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                    <div class="panel-heading text-right" style="background-color: white; padding: 5px; padding-right: 20px; font-size: 5px;">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#data-table">
                            <button class="btn btn-default btn-xs"><span class="fa fa-caret-down"></span></button>
                        </a>
                      </h4>
                    </div>
                    <div id="data-table" class="panel-collapse collapse in">
                            <div class="container-fluid" style="margin-top: 15px;">
                                <div class="row">
                                        <div class="col-sm-3">
                                            <form action="" method="post">
                                                <div class="input-group">
                                                    <input type="text" name="inputan_pencarian" class="form-control" placeholder="Cari data..." autocomplete="false" autofocus>
                                                    <span class="input-group-btn">
                                                        <input type="submit" name="cari" class="btn btn-warning" value="Cari">
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-3 text-right" style="padding-top: 5px;">
                                            <a href="#"><button class="btn btn-danger btn-xs"><span class="fa fa-print"></span> Print</button></a>
                                            <a href="?page=s_masuk&action=create_surat_masuk"><button class="btn btn-success btn-xs"><span class="fa fa-plus"></span> Tambah data</button></a>
                                        </div>
                                </div>
                            </div>
                            <div class="container-fluid" style="margin-top: 20px;">
                                    <div class="table table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr class="info">
                                                    <th class="text-center" width="10">No.</th>
                                                    <th class="text-center" width="10">Nomor Agenda</th>
                                                    <th class="text-center" width="150">Jenis Surat</th>
                                                    <th class="text-center" width="150">Tanggal Kirim</th>
                                                    <th class="text-center">Tanggal Terima</th>
                                                    <th class="text-center" width="150">Nomor Surat</th>
                                                    <th class="text-center" width="150">Pengirim</th>
                                                    <th class="text-center">Perihal</th>
                                                    <th class="text-center" width="100">Status disposisi</th>
                                                    <th class="text-center">Opsi</th>
                                                </tr>
                                            </thead>

                                            <?php
                                                $keyword    = @$_POST['inputan_pencarian'];
                                                $btn_cari   = @$_POST['cari'];

                                                // pencarian
                                                if ($btn_cari) {
                                                    if ($keyword != "") {
                                                        $sql = mysqli_query($conn, "SELECT * FROM tb_surat_masuk WHERE no_agenda LIKE '%$keyword%' or
                                                                                                                        jenis_surat LIKE '%$keyword%' or
                                                                                                                        tanggal_kirim LIKE '%$keyword%' or
                                                                                                                        tanggal_terima LIKE '%$keyword%' or
                                                                                                                        no_surat LIKE '%$keyword%' or
                                                                                                                        pengirim LIKE '%$keyword%' or
                                                                                                                        perihal LIKE '%$keyword%'") or die (mysqli_error());
                                                    }else{
                                                        $sql = mysqli_query($conn, "SELECT * FROM tb_surat_masuk") or die (mysqli_error());
                                                        // untuk status dispo surat
                                                        // $sql = mysqli_query($conn, "SELECT * FROM tb_surat_masuk") or die (mysqli_error());
                                                        // $data_dispo = mysqli_fetch_array($sql);
                                                    }
                                                }else{
                                                    $sql = mysqli_query($conn, "SELECT * FROM tb_surat_masuk") or die (mysqli_error());
                                                }

                                                $cek = mysqli_num_rows($sql);
                                                if($cek < 1){
                                                    ?>
                                                        <tr>
                                                            <td colspan="10" align="center"> Data tidak ada !</td>
                                                        </tr>
                                                    <?php
                                                }
                                                else{
                                                    $no = 1;
                                                    while($data = mysqli_fetch_array($sql)){
                                            ?>

                                                <tbody>
                                                    <tr>
                                                        <td><?= $no++; ?>.</td>
                                                        <!-- <td class="text-center"><img class="img-circle" width="80" src="petugas/img/<?= $data['foto_usr']; ?>" alt="<?= $data['foto_usr']; ?>"></td> -->
                                                        <td><?= $data['no_agenda']; ?></td>
                                                        <td>
                                                            <?php
                                                                if ($data['jenis_surat'] === 'B0') {
                                                                    echo "SURAT KELUAR INTERN";
                                                                }else if ($data['jenis_surat'] === 'B1') {
                                                                    echo "SURAT KELUAR EKSTERN";
                                                                }else if ($data['jenis_surat'] === 'SI') {
                                                                    echo "SURAT INSTRUKSI";
                                                                }else if ($data['jenis_surat'] === 'SK') {
                                                                    echo "SURAT KEPUTUSAN";
                                                                }else if ($data['jenis_surat'] === 'SP') {
                                                                    echo "SURAT PERINGATAN";
                                                                }else if ($data['jenis_surat'] === 'SPE') {
                                                                    echo "SURAT PENGUMUMAN";
                                                                }else if ($data['jenis_surat'] === 'STG') {
                                                                    echo "SURAT TEGURAN";
                                                                }
                                                            ?>
                                                        </td>
                                                        <td><?= $data['tanggal_kirim']; ?></td>
                                                        <td><?= $data['tanggal_terima']; ?></td>
                                                        <td><?= $data['no_surat']; ?></td>
                                                        <td><?= $data['pengirim']; ?></td>
                                                        <td><?= $data['perihal']; ?></td>
                                                        <td class="text-center">
                                                            <?php
                                                                if ($data['status'] == 'B') {
                                                                    ?>
                                                                        <a href="?page=disposisi&action=create_disposisi&no_agenda=<?= $data['no_agenda']; ?>" class="btn btn-link"><b>Disposisi</b></a>
                                                                    <?php
                                                                }else if ($data['status'] == 'S') {
                                                                    ?>
                                                                        <font style="font-size: 20px; color: green;"><i class="fa fa-check-circle"></i></font>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </td>
                                                        <td class="text-center">

                                                            <!-- Modal -->
                                                            <!-- <a href="modal.php?&id=<?php echo $data['id']; ?>" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal" style="background-color: black; color: white;"><span class="fa fa-eye"></span></a> -->
                                                            <!-- <?php //include 'modal.php'; ?> -->
                                                            <!-- end modal -->

                                                            <a href="?page=s_masuk&action=update_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>" class="btn btn-primary btn-xs"><span class="fa fa-edit"></span></a>
                                                            <a href="?page=s_masuk&action=delete_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>" onclick="return confirm('Yakin ingin menghapus data ?')" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </table>

                                        <div class="col-sm-6">
                                            <h5>Jumlah data
                                                <span class="label label-default">
                                                    <?php
                                                        $sql = "SELECT * FROM tb_surat_masuk ORDER BY no_agenda DESC";

                                                        if ($total = mysqli_query($conn, $sql)) {
                                                            $row = mysqli_num_rows($total);
                                                            echo $row;
                                                        }
                                                    ?>
                                                </span>
                                            </h5>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <!-- <ul class="pagination">
                                                <li><a href="#">&laquo;</a></li>
                                                <li><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#">&raquo;</a></li>
                                            </ul> -->
                                        </div>
                                    </div>
                            </div>
                    </div>
                  </div>
                </div>
            </div>
    </div>
</div>