<div class="row">
	<div class="col-sm-12">
		<h3></h3><br>
		<ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
		  <li><a href="?page=s_masuk"><i class="fa fa-user"></i> Data surat masuk</a></li>
		  <li class="active">Edit data surat masuk</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1 canvas">
		<div class="col-sm-12">
			<div class="panel panel-success">
			  	<div class="panel-heading" style="background-color: #0099FF; color: white;">
			    	<h3 class="panel-title"><span class="fa fa-envelope"> Edit surat masuk</span></h3>
			  	</div>

			  		<?php
			  			$no_agenda		= @$_GET['no_agenda'];
			  			$file 			= @$_GET['file'];
			  			$sql			= mysqli_query($conn, "SELECT * FROM tb_surat_masuk WHERE no_agenda ='$no_agenda'") or die (mysqli_error());
			  			$data 			= mysqli_fetch_array($sql);
			  		?>

			  	<form method="post" role="form" action="" class="form-horizontal" enctype="multipart/form-data">
				<div class="panel-body">
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="noagenda">No Agenda</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="no_agenda" class="form-control" id="noagenda" value="<?= $data['no_agenda'] ?>" readonly>
				    	  </div>
				    	</div>
				    	<!-- <div class="form-group"> -->
				    	  <!-- <div class="col-sm-10"> -->
				    	  	<input type="hidden" name="id" class="col-sm-3 form-control" id="id" value="<?= $data['no_agenda'] ?>">
				    	  <!-- </div> -->
				    	<!-- </div> -->
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="jsurat">Jenis Surat</label>
				    		<div class="col-sm-10">
					    	  <select name="j_surat" class="form-control" id="jsurat">
					    	    <?php
					    	   	if ($data['jenis_surat'] == 'B0') {
					    	   		?>
					    	   			<option value="B0">SURAT KELUAR INTERN</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'B1') {
					    	   		?>
					    	   			<option value="B1">SURAT KELUAR EKSTERN</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'SI') {
					    	   		?>
					    	   			<option value="SI">SURAT INSTRUKSI</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'SK') {
					    	   		?>
					    	   			<option value="SK">SURAT KEPUTUSAN</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'SP') {
					    	   		?>
					    	   			<option value="SP">SURAT PERINGATAN</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'SPE') {
					    	   		?>
					    	   			<option value="SPE">SURAT PENGUMUMAN</option>
					    	   		<?php
					    	   	}else if ($data['jenis_surat'] == 'STG') {
					    	   		?>
					    	   			<option value="STG">SURAT TEGURAN</option>
					    	   		<?php
					    	   	}
					    	    ?>
					    	    <option value="B0">SURAT KELUAR INTERN</option>
					    	    <option value="B1">SURAT KELUAR EKSTERN</option>
					    	    <option value="SI">SURAT INSTRUKSI</option>
					    	    <option value="SK">SURAT KEPUTUSAN</option>
					    	    <option value="SP">SURAT PERINGATAN</option>
					    	    <option value="SPE">SURAT PENGUMUMAN</option>
					    	    <option value="STG">SURAT TEGURAN</option>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="tglkirim">Tanggal Kirim</label>
				    	  <div class="col-sm-10">
				    	  	<input type="date" name="tgl_kirim" class="form-control" id="tglkirim" value="<?= $data['tanggal_kirim'] ?>">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="tglterima">Tanggal Terima</label>
				    	  <div class="col-sm-10">
				    	  	<input type="date" name="tgl_terima" class="form-control" id="tglterima" value="<?= $data['tanggal_terima'] ?>">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="nosmasuk">No surat</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="no_smasuk" class="form-control" id="nosmasuk" value="<?= $data['no_surat'] ?>" readonly>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="pengirim">Pengirim</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="pengirim" class="form-control" id="pengirim" value="<?= $data['pengirim'] ?>">
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="Perihal">Perihal</label>
				    	  	<div class="col-sm-10 ">
				    	  		<textarea type="text" name="perihal" rows="6" class="form-control" id="Perihal" value="<?= $data['perihal'] ?>"><?= $data['perihal'] ?></textarea>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="file">File</label>
				    		<div class="col-sm-10">
				    			<div class="col-sm-6">
						    	    <input type="file" name="file" id="filr">
						    	    <p class="help-block">Gunakan file ber-ekstensi docx / jpg.</p>
						    	</div>
						    	<div class="col-sm-6">
						    		<div class="col-sm-12">
						    			<p><mark><?= $file; ?></mark></p>
						    		</div>
						    	</div>
					    	</div>
				    	</div>
				</div>
			  	<div class="panel-footer text-right" style="background-color: #fff;">
			  		<input type="submit" name="edit" class="btn btn-success" value="Simpan">
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  	</form>

			  	<?php
			  		$file = @$_GET['file'];
				  	$folder = "surat_masuk/file_surat/";
			  		$noagenda   = htmlspecialchars(@$_POST['no_agenda']);
			  		$id  		= htmlspecialchars(@$_POST['id']);
			  		$j_surat   	= htmlspecialchars(@$_POST['j_surat']);
			  		$tgl_kirim  = htmlspecialchars(@$_POST['tgl_kirim']);
			  		$tgl_terima = htmlspecialchars(@$_POST['tgl_terima']);
			  		$no_smasuk  = htmlspecialchars(@$_POST['no_smasuk']);
			  		$pengirim  	= htmlspecialchars(@$_POST['pengirim']);
			  		$perihal  	= htmlspecialchars(@$_POST['perihal']);

			  		// upload file (file)
			  		$nama_file				= @$_FILES['file']['name'];
			  		$nama_file_baru			= "ARSPSRT_".$nama_file;
			  		$file_tmp 				= @$_FILES['file']['tmp_name'];
			  		$ukuran					= @$_FILES['file']['size'];
			  		$x 						= explode('.', $nama_file_baru);
			  		$ekstensi				= strtolower(end($x));
			  		$ekstensi_diperbolehkan	= array('docx','jpg','jpeg');
			  		$path					= 'surat_masuk/file_surat/';

			  		$edit_data  = @$_POST['edit'];

			  		if($edit_data){
			  		    if($noagenda == "" || $j_surat == "" || $tgl_kirim == "" || $tgl_terima == "" || $no_smasuk == "" || $pengirim == "" || $perihal == ""){
			  		        ?>
			  		            <script type="text/javascript">
			  		                alert("Data tidak boleh kosong !");
			  		                window.location.href ="?page=s_masuk&action=update_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>";
			  		            </script>
			  		        <?php
			  		    }
			  		    else{
			  		    	if ($nama_file == "") {
			  		    		mysqli_query($conn, "UPDATE tb_surat_masuk SET jenis_surat='$j_surat', tanggal_kirim='$tgl_kirim', tanggal_terima='$tgl_terima', pengirim='$pengirim', perihal='$perihal' WHERE no_agenda='$noagenda'") or die (mysqli_error());
				  		    	?>
				  		    		<script type="text/javascript">
				  		    			alert("Data berhasi deiedit !");
				  		    			window.location.href="?page=s_masuk";
				  		    		</script>
				  		    	<?php
			  		    	} else {
			  		    		if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
			  		    			if ($ukuran < 2000000) {
			  		    				unlink($folder.$file);
			  		    				$pindah_foto = move_uploaded_file($file_tmp, $path.$nama_file_baru);
			  		    				if($pindah_foto){
			  		    					mysqli_query($conn, "UPDATE tb_surat_masuk SET jenis_surat='$j_surat', tanggal_kirim='$tgl_kirim', tanggal_terima='$tgl_terima', pengirim='$pengirim', perihal='$perihal', file='$nama_file_baru' WHERE no_agenda='$noagenda'") or die (mysqli_error());
			  		    						?>
			  		    							<script type="text/javascript">
			  		    								alert("Data berhasi diedit !");
			  		    								window.location.href="?page=s_masuk";
			  		    							</script>
			  		    						<?php
			  		    				}
			  		    				else{
			  		    					?>
			  		    						<script type="text/javascript">
			  		    							alert("File Gagal diupload !");
			  		    							window.location.href="?page=s_masuk&action=update_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>";
			  		    						</script>
			  		    					<?php
			  		    				}
			  		    			}else{
			  		    				?>
			  		    					<script type="text/javascript">
			  		    						alert("Size terlalu besar !");
			  		    						window.location.href="?page=s_masuk&action=update_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>";
			  		    					</script>
			  		    				<?php
			  		    			}
			  		    		}else{
			  		    			?>
			  		    				<script type="text/javascript">
			  		    					alert("File tidak diperbolehkan !");
			  		    					window.location.href="?page=s_masuk&action=update_surat_masuk&no_agenda=<?= $data['no_agenda']; ?>&file=<?php echo $data['file'] ?>";
			  		    				</script>
			  		    			<?php
			  		    		}
			  		    	}
				  		}
			  		}//end tambahdata
			  	?>

			</div>
		</div>
	</div>
</div>