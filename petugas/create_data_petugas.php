<div class="row">
	<div class="col-sm-12">
		<h3></h3><br>
		<ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
		  <li><a href="?page=petugas"><i class="fa fa-user"></i> Data Petugas</a></li>
		  <li class="active">Tambah data petugas</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2 canvas">
		<div class="col-sm-12">
			<div class="panel panel-success">
			  	<div class="panel-heading" style="background-color: #0099FF; color: white;">
			    	<h3 class="panel-title"><span class="fa fa-user-plus"> Tambah data petugas</span></h3>
			  	</div>

					<?php
					  $carikode = mysqli_query($conn, "SELECT MAX(id) from tb_petugas") or die (mysqli_error($conn));
					  $datakode = mysqli_fetch_array($carikode);
					  if($datakode){
					    $nilaikode = substr($datakode[0], 4);
					    $kode = (int) $nilaikode;
					    $kode = $kode + 1;
					    $hasilkode = "PTGS".str_pad($kode, 4, "0", STR_PAD_LEFT);
					  }
					  else{
					    $hasilkode = "PTGS0001";
					  }
					?>

			  	<form method="post" role="form" action="" class="form-horizontal" enctype="multipart/form-data" name="tambah">
				<div class="panel-body">
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="id">Id</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="id" class="form-control" id="id" value="<?= $hasilkode; ?>" readonly>
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="n_depan">Nama</label>
				    	  <div class="col-sm-5">
				    	  	<input type="text" name="n_depan" class="col-sm-3 form-control" id="n_depan" placeholder="Masukan nama depan" autofocus="">
				    	  </div>
						  <div class="col-sm-5">
				    	  	<input type="text" name="n_belakang" class="col-sm-3 form-control" id="n_depan" placeholder="Masukan nama belakang" autofocus="">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="jk">Jenin Kelamin</label>
				    		<div class="col-sm-10">
					    	  <select name="jk" class="form-control" id="jk">
					    	    <option>-- Jenis Kelamin --</option>
					    	    <option value="L">Laki-laki</option>
					    	    <option value="P">Perempuan</option>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="username">Username</label>
				    	  <div class="col-sm-10">
				    	  	<input type="text" name="username" class="form-control" id="username" placeholder="Masukan username">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="pass">Password</label>
				    	  <div class="col-sm-10">
				    	  	<input type="password" name="password" class="form-control" id="pass" placeholder="Masukan password">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="hak">Hak</label>
				    	  <div class="col-sm-10">
					    	  <select name="hak" class="form-control" id="hak">
					    	    <option>-- Pilih Hak akses --</option>
					    	    <option value="Administrator">Administrator</option>
					    	    <option value="User">User</option>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="foto">Foto</label>
				    		<div class="col-sm-10">
					    	    <input type="file" name="gambar" id="foto">
					    	    <p class="help-block">Gunakan gambar ber-ekstensi PNG / JPG.</p>
					    	</div>
				    	</div>
				</div>
			  	<div class="panel-footer text-right" style="background-color: #fff;">
			  		<input type="submit" name="tambah" class="btn btn-success" value="Tambah">
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  	</form>

			  	<?php
			  		$id   	= htmlspecialchars(@$_POST['id']);
			  		$nd  	= htmlspecialchars(@$_POST['n_depan']);
			  		$nb   	= htmlspecialchars(@$_POST['n_belakang']);
			  		$jk  	= htmlspecialchars(@$_POST['jk']);
			  		$usr  	= htmlspecialchars(@$_POST['username']);
			  		$pass  	= htmlspecialchars(@$_POST['password']);
			  		$hak  	= htmlspecialchars(@$_POST['hak']);

			  		// upload file (gambar)
			  		$nama_file				= @$_FILES['gambar']['name'];
			  		$nama_file_baru			= "ARSPSRT_".$nama_file;
			  		$file_tmp 				= @$_FILES['gambar']['tmp_name'];
			  		$ukuran					= @$_FILES['gambar']['size'];
			  		$x 						= explode('.', $nama_file_baru);
			  		$ekstensi				= strtolower(end($x));
			  		$ekstensi_diperbolehkan	= array('png','jpg','jpeg');
			  		$path					= 'petugas/img/';

			  		$tambah_data  = @$_POST['tambah'];

			  		if($tambah_data){
			  		    if($id == "" || $nb == "" || $nd == "" || $jk == "" || $usr == "" || $pass == "" || $hak == ""){
			  		        ?>
			  		            <script type="text/javascript">
			  		                alert("Data tidak boleh kosong !");
			  		                window.location.href ="?page=petugas&action=create_petugas";
			  		            </script>
			  		        <?php
			  		    }
			  		    else{
			  		    	if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
			  		    		if ($ukuran < 2000000) {
			  		    			$pindah_foto = move_uploaded_file($file_tmp, $path.$nama_file_baru);
			  		    			if($pindah_foto){
			  		    				mysqli_query($conn, "INSERT INTO tb_petugas values('$id', '$nd', '$nb', '$jk', '$usr', '$pass', '$hak', '$nama_file_baru')") or die (mysqli_error());
			  		    					?>
			  		    						<script type="text/javascript">
			  		    							alert("Data berhasi tersimpan !");
			  		    							window.location.href="?page=petugas";
			  		    						</script>
			  		    					<?php
			  		    			}
			  		    			else{
			  		    				?>
			  		    					<script type="text/javascript">
			  		    						alert("Gambar Gagal diupload !");
			  		    						window.location.href="?page=petugas&action=create_petugas";
			  		    					</script>
			  		    				<?php
			  		    			}
			  		    		}else{
			  		    			?>
			  		    				<script type="text/javascript">
			  		    					alert("Size terlalu besar !");
			  		    					window.location.href="?page=petugas&action=create_petugas";
			  		    				</script>
			  		    			<?php
			  		    		}
			  		    	}else{
			  		    		?>
			  		    			<script type="text/javascript">
			  		    				alert("Gaambar tidak diperbolehkan !");
			  		    				window.location.href="?page=petugas&action=create_petugas";
			  		    			</script>
			  		    		<?php
			  		    	}
			  		    }
			  		}//end tambahdata
			  	?>

			</div>
		</div>
	</div>
</div>