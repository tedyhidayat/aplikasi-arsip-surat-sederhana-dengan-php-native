<div class="row">
	<div class="col-sm-12">
		<h3></h3><br>
		<ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
		  <li><a href="?page=petugas"><i class="fa fa-user"></i> Data Petugas</a></li>
		  <li class="active">Edit data petugas</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2 canvas">
		<div class="col-sm-12">
			<div class="panel panel-success">
			  	<div class="panel-heading" style="background-color: #0099FF; color: white;">
			    	<h3 class="panel-title"><span class="fa fa-edit"> Edit data petugas</span></h3>
			  	</div>

			  	<?php
			  		$id_petugas 	= @$_GET['id_petugas'];
			  		$foto 			= @$_GET['foto'];
			  		$sql			= mysqli_query($conn, "SELECT * FROM tb_petugas WHERE id ='$id_petugas'") or die (mysqli_error());
			  		$data 			= mysqli_fetch_array($sql);
			  	?>

			  	<form method="post" role="form" class="form-horizontal" enctype="multipart/form-data">
				<div class="panel-body">
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="id">Id</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="id" class="form-control" id="id" value="<?= $data['id'] ?>" readonly>
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="n_depan">Nama</label>
				    	  <div class="col-sm-5">
				    	  	<input type="text" name="n_depan" class="col-sm-3 form-control" id="n_depan" placeholder="Masukan nama depan" value="<?= $data['nama_depan'] ?>" autofocus="">
				    	  </div>
						  <div class="col-sm-5">
				    	  	<input type="text" name="n_belakang" class="col-sm-3 form-control" id="n_depan" placeholder="Masukan nama belakang" value="<?= $data['nama_belakang'] ?>" autofocus="">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="jk">Jenin Kelamin</label>
				    		<div class="col-sm-10">
					    	  <select name="jk" class="form-control" id="jk">
					    	     	<?php
					    	    		if ($data['j_kelamin'] == 'L') {
					    	    			?>
					    	    				<option value="L">Laki-laki</option>
					    	    				<option value="P">Perempuan</option>
					    	    			<?php
					    	    		}else{
					    	    			?>
					    	    				<option value="P">Perempuan</option>
					    	    				<option value="L">Laki-laki</option>
					    	    			<?php
					    	    		}
					    	    	?>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="username">Username</label>
				    	  <div class="col-sm-10">
				    	  	<input type="text" name="username" class="form-control" id="username" placeholder="Masukan username"  value="<?= $data['username'] ?>">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="pass">Password</label>
				    	  <div class="col-sm-10">
				    	  	<input type="password" name="password" class="form-control" id="pass" placeholder="Masukan password"  value="<?= $data['password']; ?>">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="hak">Hak</label>
				    	  <div class="col-sm-10">
					    	  <select name="hak" class="form-control" id="hak">
					    	   	<?php
					    	  		if ($data['hak'] == 'Administrator') {
					    	  			?>
					    	  				<option value="Administrator">Administrator</option>
					    	  				<option value="User">User</option>
					    	  			<?php
					    	  		}else{
					    	  			?>
					    	  				<option value="User">User</option>
					    	  				<option value="Administrator">Administrator</option>
					    	  			<?php
					    	  		}
					    	  	?>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group" style="margin-top: 50px;">
				    		<label class="col-sm-2 control-label" for="foto">Foto</label>
				    		<div class="col-sm-10">
				    			<div class="col-sm-6">
						    	    <input type="file" name="gambar" id="foto">
						    	    <p class="help-block">Gunakan gambar ber-ekstensi PNG / JPG.</p>
						    	</div>
						    	<div class="col-sm-6">
						    		<img width="150" src="petugas/img/<?= $foto; ?>" alt="<?= $foto; ?>" class="thumbnail">
						    	</div>
					    	</div>
				    	</div>
				</div>
			  	<div class="panel-footer text-right" style="background-color: #fff;">
			  		<input type="submit" name="edit" class="btn btn-primary" value="Edit">
			  		<a href="?page=petugas" class="btn btn-danger">Cancel</a>
			  	</div>
			  	</form>

			  	<?php
			  		$file = @$_GET['foto'];
				  	$folder = "petugas/img/";

			  		$id   	= htmlspecialchars(@$_POST['id']);
			  		$nd  	= htmlspecialchars(@$_POST['n_depan']);
			  		$nb   	= htmlspecialchars(@$_POST['n_belakang']);
			  		$jk  	= htmlspecialchars(@$_POST['jk']);
			  		$usr  	= htmlspecialchars(@$_POST['username']);
			  		$pass  	= htmlspecialchars(@$_POST['password']);
			  		$hak  	= htmlspecialchars(@$_POST['hak']);

			  		// upload file (gambar)
			  		$nama_file				= @$_FILES['gambar']['name'];
			  		$nama_file_baru			= "ARSPSRT_".$nama_file;
			  		$file_tmp 				= @$_FILES['gambar']['tmp_name'];
			  		$ukuran					= @$_FILES['gambar']['size'];
			  		$x 						= explode('.', $nama_file_baru);
			  		$ekstensi				= strtolower(end($x));
			  		$ekstensi_diperbolehkan	= array('png','jpg','jpeg');
			  		$path					= 'petugas/img/';

			  		$edit_data  = @$_POST['edit'];

			  		if($edit_data){
			  		    if($id == "" || $nb == "" || $nd == "" || $jk == "" || $usr == "" || $pass == "" || $hak == ""){
			  		        ?>
			  		            <script type="text/javascript">
			  		                alert("Data tidak boleh kosong !");
			  		                window.location.href ="?page=petugas&action=update_petugas&id_petugas=<?= $data['id']; ?>&foto=<?php echo $data['foto_usr'] ?>";
			  		            </script>
			  		        <?php
			  		    }
			  		    else{
			  		    	if ($nama_file == "") {
				  		    	mysqli_query($conn, "UPDATE tb_petugas SET nama_depan='$nd', nama_belakang='$nb', j_kelamin='$jk', username='$usr', password=sha1('$pass'), hak='$hak' WHERE id='$id'") or die (mysqli_error());
				  		    	?>
				  		    		<script type="text/javascript">
				  		    			alert("Data berhasi deiedit !");
				  		    			window.location.href="?page=petugas";
				  		    		</script>
				  		    	<?php
				  		    }else{
				  		    	if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
				  		    		if ($ukuran < 2000000) {
				  		    			unlink($folder.$file);
				  		    			$pindah_foto = move_uploaded_file($file_tmp, $path.$nama_file_baru);
				  		    			if($pindah_foto){
				  		    				mysqli_query($conn, "UPDATE tb_petugas SET nama_depan='$nd', nama_belakang='$nb', j_kelamin='$jk', username='$usr', password=sha1('$pass'), hak='$hak', foto_usr='$nama_file_baru' WHERE id='$id'") or die (mysqli_error());
				  		    					?>
				  		    						<script type="text/javascript">
				  		    							alert("Data berhasi tersimpan !");
				  		    							window.location.href="?page=petugas";
				  		    						</script>
				  		    					<?php
				  		    			}
				  		    			else{
				  		    				?>
				  		    					<script type="text/javascript">
				  		    						alert("Gambar Gagal diupload !");
				  		    						window.location.href="?page=petugas&action=update_petugas&id_petugas=<?= $data['id']; ?>&foto=<?php echo $data['foto_usr'] ?>";
				  		    					</script>
				  		    				<?php
				  		    			}
				  		    		}else{
				  		    			?>
				  		    				<script type="text/javascript">
				  		    					alert("Size terlalu besar !");
				  		    					window.location.href="?page=petugas&action=update_petugas&id_petugas=<?= $data['id']; ?>&foto=<?php echo $data['foto_usr'] ?>";
				  		    				</script>
				  		    			<?php
				  		    		}
				  		    	}else{
				  		    		?>
				  		    			<script type="text/javascript">
				  		    				alert("Gaambar tidak diperbolehkan !");
				  		    				window.location.href="?page=petugas&action=update_petugas&id_petugas=<?= $data['id']; ?>&foto=<?php echo $data['foto_usr'] ?>";
				  		    			</script>
				  		    		<?php
				  		    	}
				  		    }
			  		    }
			  		}//end tambahdata
			  	?>

			</div>
		</div>
	</div>
</div>