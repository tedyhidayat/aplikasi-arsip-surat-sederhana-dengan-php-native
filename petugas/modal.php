<?php
  $id    = @$_GET['id'];
  $sql   = mysqli_query($conn, "SELECT * FROM tb_petugas WHERE id ='$id'") or die (mysqli_error());
  $data_user  = mysqli_fetch_array($sql);
?>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Profile</h4>
      </div>
      <div class="modal-body">
        <p><img class="img-circle" width="80" src="petugas/img/<?= $data['foto_usr']; ?>" alt="<?= $data['foto_usr']; ?>"></p>
        <p><?= $data_user['id']; ?></p>
        <p><?= $data_user['nama_depan']; ?> <?= $data['nama_belakang']; ?></p>
        <p><?= $data_user['j_kelamin']; ?></p>
        <p><?= $data_user['hak']; ?></p>
      </div>
      <div class="modal-footer">
        <a href="?page=petugas&action=update_petugas&id_petugas=<?= $data['id']; ?>&foto=<?php echo $data['foto_usr'] ?>"><button type="button" class="btn btn-primary">Edit</button></a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- end modal content -->

  </div>
</div>