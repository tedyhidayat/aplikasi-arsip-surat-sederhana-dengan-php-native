<div class="row">
	<div class="col-sm-12">
		<h3></h3><br>
		<ol class="breadcrumb" style="background-color: #fff; box-shadow: 0px 0px 3px gray;">
		  <li><a href="?page=s_keluar"><i class="fa fa-paper-plan"></i> Data surat keluar</a></li>
		  <li class="active">Tulis surat keluar</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-10 col-sm-offset-1 canvas">
		<div class="col-sm-12">
			<div class="panel panel-success">
			  	<div class="panel-heading" style="background-color: #0099FF; color: white;">
			    	<h3 class="panel-title"><span class="fa fa-envelope"> Buat surat keluar</span></h3>
			  	</div>

			  		<?php
			  		  $carikode = mysqli_query($conn, "SELECT MAX(no_agenda) from tb_surat_keluar") or die (mysqli_error($conn));
			  		  $datakode = mysqli_fetch_array($carikode);
			  		  if($datakode){
			  		    $nilaikode = substr($datakode[0], 1);
			  		    $kode = (int) $nilaikode;
			  		    $kode = $kode + 1;
			  		    $noagenda = str_pad($kode, 10, "0", STR_PAD_LEFT);
			  		  }
			  		  else{
			  		    $noagenda = "0000000001";
			  		  }
			  		?>

					<?php
					  $carikode = mysqli_query($conn, "SELECT MAX(no_surat) from tb_surat_keluar") or die (mysqli_error($conn));
					  $datakode = mysqli_fetch_array($carikode);
					  if($datakode){
					    $nilaikode = substr($datakode[0], 6);
					    $kode = (int) $nilaikode;
					    $kode = $kode + 1;
					    $noskeluar = "SRT-K/".str_pad($kode, 4, "0", STR_PAD_LEFT);
					  }
					  else{
					    $noskeluar = "SRT-K/0001";
					  }
					?>

			  	<form method="post" role="form" action="" class="form-horizontal" enctype="multipart/form-data">
				<div class="panel-body">
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="noagenda">No Agenda</label>
				    	  <div class="col-sm-10 ">
				    	  	<input type="text" name="no_agenda" class="form-control" id="noagenda" value="<?= $noagenda; ?>" readonly>
				    	  </div>
				    	</div>
				    	<!-- <div class="form-group"> -->
				    	  <!-- <div class="col-sm-10"> -->
				    	  	<input type="hidden" name="id" class="col-sm-3 form-control" id="id" value="<?= $data_user['id']; ?>">
				    	  <!-- </div> -->
				    	<!-- </div> -->
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="jsurat">Jenis Surat</label>
				    		<div class="col-sm-10">
					    	  <select name="j_surat" class="form-control" id="jsurat">
					    	    <option>-- Jenis Surat --</option>
					    	    <option value="B0">SURAT KELUAR INTERN</option>
					    	    <option value="B1">SURAT KELUAR EKSTERN</option>
					    	    <option value="SI">SURAT INSTRUKSI</option>
					    	    <option value="SK">SURAT KEPUTUSAN</option>
					    	    <option value="SP">SURAT PERINGATAN</option>
					    	    <option value="SPE">SURAT PENGUMUMAN</option>
					    	    <option value="STG">SURAT TEGURAN</option>
					    	  </select>
					      </div>
				    	</div>
				    	<div class="form-group">
				    	  <label class="col-sm-2 control-label" for="tglkirim">Tanggal Kirim</label>
				    	  <div class="col-sm-10">
				    	  	<input type="date" name="tgl_kirim" class="form-control" id="tglkirim">
				    	  </div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="noskeluar">No surat</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="no_skeluar" class="form-control" id="noskeluar" value="<?= $noskeluar; ?>" readonly>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="pengirim">Pengirim</label>
				    	  	<div class="col-sm-10 ">
				    	  		<input type="text" name="pengirim" class="form-control" id="pengirim" placeholder="Pengirim">
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    	  	<label class="col-sm-2 control-label" for="Perihal">Perihal</label>
				    	  	<div class="col-sm-10 ">
				    	  		<textarea type="text" name="perihal" rows="6" class="form-control" id="Perihal" placeholder="Perihal"></textarea>
				    	  	</div>
				    	</div>
				    	<div class="form-group">
				    		<label class="col-sm-2 control-label" for="fil">File</label>
				    		<div class="col-sm-10">
					    	    <input type="file" name="file" id="file">
					    	    <p class="help-block">Gunakan gambar ber-ekstensi PDF / JPG.</p>
					    	</div>
				    	</div>
				</div>
			  	<div class="panel-footer text-right" style="background-color: #fff;">
			  		<input type="submit" name="tambah" class="btn btn-success" value="Buat surat">
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  	</form>

			  	<?php
			  		$noagenda   = htmlspecialchars(@$_POST['no_agenda']);
			  		$id  		= htmlspecialchars(@$_POST['id']);
			  		$j_surat   	= htmlspecialchars(@$_POST['j_surat']);
			  		$tgl_kirim  = htmlspecialchars(@$_POST['tgl_kirim']);
			  		$no_skeluar = htmlspecialchars(@$_POST['no_skeluar']);
			  		$pengirim  	= htmlspecialchars(@$_POST['pengirim']);
			  		$perihal  	= htmlspecialchars(@$_POST['perihal']);

			  		// upload file (file)
			  		$nama_file				= @$_FILES['file']['name'];
			  		$nama_file_baru			= "ARSPSRT_".$nama_file;
			  		$file_tmp 				= @$_FILES['file']['tmp_name'];
			  		$ukuran					= @$_FILES['file']['size'];
			  		$x 						= explode('.', $nama_file_baru);
			  		$ekstensi				= strtolower(end($x));
			  		$ekstensi_diperbolehkan	= array('docx','jpg','jpeg');
			  		$path					= 'surat_keluar/file_surat/';

			  		$tambah_data  = @$_POST['tambah'];

			  		if($tambah_data){
			  		    if($noagenda == "" || $j_surat == "" || $tgl_kirim == "" || $no_skeluar == "" || $pengirim == "" || $perihal == "" || $nama_file_baru == ""){
			  		        ?>
			  		            <script type="text/javascript">
			  		                alert("Data tidak boleh kosong !");
			  		                window.location.href ="?page=s_keluar&action=create_surat_keluar";
			  		            </script>
			  		        <?php
			  		    }
			  		    else{
			  		    	if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
			  		    		if ($ukuran < 2000000) {
			  		    			$pindah_foto = move_uploaded_file($file_tmp, $path.$nama_file_baru);
			  		    			if($pindah_foto){
			  		    				mysqli_query($conn, "INSERT INTO tb_surat_keluar(no_agenda,id,jenis_surat,tanggal_kirim,no_surat,pengirim,perihal,file) values('$noagenda', '$id', '$j_surat', '$tgl_kirim', '$no_skeluar', '$pengirim', '$perihal', '$nama_file_baru')") or die (mysqli_error($conn));
			  		    					?>
			  		    						<script type="text/javascript">
			  		    							alert("Data berhasi tersimpan !");
			  		    							window.location.href="?page=s_keluar";
			  		    						</script>
			  		    					<?php
			  		    			}
			  		    			else{
			  		    				?>
			  		    					<script type="text/javascript">
			  		    						alert("File Gagal diupload !");
			  		    						window.location.href="?page=s_keluar&action=create_surat_keluar";
			  		    					</script>
			  		    				<?php
			  		    			}
			  		    		}else{
			  		    			?>
			  		    				<script type="text/javascript">
			  		    					alert("Size terlalu besar !");
			  		    					window.location.href="?page=s_keluar&action=create_surat_keluar";
			  		    				</script>
			  		    			<?php
			  		    		}
			  		    	}else{
			  		    		?>
			  		    			<script type="text/javascript">
			  		    				alert("File tidak diperbolehkan !");
			  		    				window.location.href="?page=s_keluar&action=create_surat_keluar";
			  		    			</script>
			  		    		<?php
			  		    	}
			  		    }
			  		}//end tambahdata
			  	?>

			</div>
		</div>
	</div>
</div>