<?php
@session_start();
include '_config/config.php';

if(@$_SESSION['Administrator'] || @$_SESSION['User']){
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Halaman Utama - Arsip Surat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="_assets/css/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <link href="_assets/css/style.css" type="text/css" rel="stylesheet" />
    <link href="_assets/css/index_content.css" type="text/css" rel="stylesheet" />
    <link href="_assets/css/canvas.css" type="text/css" rel="stylesheet" />
    <link href="_assets/font/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet" />

</head>
<body>
    <?php
        if (@$_SESSION['Administrator']){
                $user_login = @$_SESSION['Administrator'];
            }
            $sql_user = mysqli_query($conn, "SELECT * FROM tb_petugas WHERE id = '$user_login'") or die (mysql_error());
            $data_user = mysqli_fetch_array($sql_user);
    ?>

    <!-------------------------------- Bagian HEADER -------------------------------->
    <div class="container-fluid navbar-fixed-top">
        <!-- <div class="row identitas">
        </div> -->
        <div class="row header">
            <div class="col-md-12 title">
                <a href="#">
                    <!-- <img src="_assets/images/1.png" alt="" class="img-circle"> -->
                    <span class="label title"><i class="fa fa-paper-plane fa-lg" style="color: cyan;"></i> _Aplikasi Arsip Surat</span>
                </a>
            </div>
        </div>
        <div class="row">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                            <span class="glyphicon glyphicon-align-justify"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="menu">
                        <ul class="nav navbar-nav">
                            <li class="active text-center home"><a href="../arsip_surat"><span class="glyphicon glyphicon-home"></span> </a></li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="?page=petugas"><span class="fa fa-users"></span> Data Petugas </a>
                                <ul class="dropdown-menu">
                                    <li><a href="?page=petugas"><span class="glyphicon glyphicon-eye-open"></span> Lihat Data Petugas</a></li>
                                    <li><a href="?page=petugas&action=create_petugas"><span class="fa fa-plus-square"></span> Tambah Petugas</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="?page=s_masuk"><span class="fa fa-envelope"></span> Surat Masuk </a>
                                <ul class="dropdown-menu">
                                    <li><a href="?page=s_masuk"><span class="glyphicon glyphicon-eye-open"></span> Lihat Data Surat</a></li>
                                    <li><a href="?page=s_masuk&action=create_surat_masuk"><span class="glyphicon glyphicon-pencil"></span> Tulis Surat Masuk</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="?page=s_keluar"><span class="fa fa-paper-plane"></span> Surat Keluar </a>
                                <ul class="dropdown-menu">
                                    <li><a href="?page=s_keluar"><span class="glyphicon glyphicon-eye-open"></span> Lihat Data Surat</a></li>
                                    <li><a href="?page=s_keluar&action=create_surat_keluar"><span class="glyphicon glyphicon-pencil"></span> Tulis Surat Keluar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="?page=disposisi"><span class="fa fa-reply"></span> DISPOSISI </a>
                                <ul class="dropdown-menu">
                                    <li><a href="?page=disposisi"><span class="glyphicon glyphicon-reply"></span> Disposisi surat masuk</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="?page=info"><span class="fa fa-info-circle"></span> Tentang Aplikasi </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <div class="user-settings-wrapper">
                                <ul class="nav">
                                    <p class="navbar-text" style="color: white;">Signed in as  <strong style="color: orange;"><u><?php echo $data_user['hak']; ?></u></strong></p>
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                            .<span class="glyphicon glyphicon-user"></span>.
                                        </a>
                                        <div class="dropdown-menu dropdown-settings">
                                            <div class="media">
                                                <a class="media-left" href="#">
                                                    <img src="petugas/img/<?= $data_user['foto_usr'];?>" alt="" class="img-rounded" />
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><?php echo $data_user['nama_depan']; ?> <?php echo $data_user['nama_belakang']; ?> </h4>
                                                    <h5><?php echo $data_user['hak']; ?></h5>

                                                </div>
                                            </div>
                                            <!-- <hr />
                                            <h5><strong>Personal Bio : </strong></h5>
                                            Anim pariatur cliche reprehen derit. -->
                                            <hr />
                                            <a href="?page=petugas&action=update_petugas&id_petugas=<?= $data_user['id']; ?>&foto=<?php echo $data_user['foto_usr'] ?>" class="btn btn-info btn-sm">Edit Profile</a> <a href="auth/logout.php" class="btn btn-danger btn-sm">Logout</a>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </ul>

                        <!-- <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pengaturan<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#"><span class="fa fa-user"></span> Akun</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><span class="fa fa-sign-out"></span> Logout</a></li>
                                    </ul>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!-------------------------------- Aakhir Header -------------------------------->

    <!-------------------------------- Bagian SECTION -------------------------------->
    <section class="sect">
        <div class="container-fluid">
            <div class="row">
                <!-- <div class="col-sm-2 text-center user-aktif">
                   <div class="thumbnail">
                     <img src="_assets/images/1.png" alt="user" class="img-circle">
                     <div class="caption">
                        <strong><?php echo $data_user['nama_depan']; ?> <?php echo $data_user['nama_belakang']; ?></strong><p>
                        <h5><?php echo $data_user['hak']; ?></h5>
                       <p><a href="#" class="btn btn-danger btn-xs" role="button">Edit profil</a></p>
                     </div>
                   </div>
                 </div> -->
                <div class="col-sm-12 content">
                        <?php
                        $page = @$_GET['page'];
                        $action = @$_GET['action'];

                            if($page == "petugas"){
                                if($action == ""){
                                    include "petugas/read_data_petugas.php";
                                }
                                else if ($action == "create_petugas"){
                                    include "petugas/create_data_petugas.php";
                                }
                                else if ($action == "update_petugas"){
                                    include "petugas/update_data_petugas.php";
                                }
                                else if ($action == "delete_petugas"){
                                    include "petugas/delete_data_petugas.php";
                                }
                            }
                            else if($page == "s_masuk"){
                                if($action == ""){
                                    include "surat_masuk/read_surat_masuk.php";
                                }
                                else if ($action == "create_surat_masuk"){
                                    include "surat_masuk/create_surat_masuk.php";
                                }
                                else if ($action == "update_surat_masuk"){
                                    include "surat_masuk/update_surat_masuk.php";
                                }
                                else if ($action == "delete_surat_masuk"){
                                    include "surat_masuk/delete_surat_masuk.php";
                                }
                            }
                            else if($page == "s_keluar"){
                                if($action == ""){
                                    include "surat_keluar/read_surat_keluar.php";
                                }
                                else if ($action == "create_surat_keluar"){
                                    include "surat_keluar/create_surat_keluar.php";
                                }
                                else if ($action == "update_surat_keluar"){
                                    include "surat_keluar/update_surat_keluar.php";
                                }
                                else if ($action == "delete_surat_keluar"){
                                    include "surat_keluar/delete_surat_keluar.php";
                                }
                            }
                            else if($page == "disposisi"){
                                if($action == ""){
                                    include "disposisi/read_disposisi.php";
                                }
                                else if ($action == "create_disposisi"){
                                    include "disposisi/create_disposisi.php";
                                }
                                else if ($action == "delete_disposisi"){
                                    include "disposisi/delete_disposisi.php";
                                }
                            }
                            else if($page == "info"){
                                include "info.php";
                            }
                            else if($page == ""){
                                include "index_content.php";
                            }
                            else{
                                echo "404 ! Halaman tidak ditemukan.";
                            }
                        ?>
                </div>
            </div>
        </div>
    </section>
    <!-------------------------------- Bagian FOOTER -------------------------------->
    <footer class="footer">
        <div class="container-fluid text-center">
            <div class="row">
                <div class="col-sm-12 copyright">
                    <h5>&copy Copyright 2018 - By Tedy Hidayat</h5>
                    <p><button class="btn btn-success btn-sm back_top">Kembali ke atas</button></p>
                </div>
            </div>
        </div>
    </footer>



<!-- Javascript file -->
<script src="_assets/css/bootstrap/js/jquery.min.js"></script>
<script src="_assets/css/bootstrap/js/bootstrap.min.js"></script>
<script src="_assets/css/bootstrap/js/back_top.js"></script>

</body>
</html>

<?php
}
else{
    header("location: auth/login.php");
}
?>