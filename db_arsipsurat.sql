-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 31, 2018 at 07:54 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_arsipsurat`
--

-- --------------------------------------------------------

--
-- Table structure for table `ket_surat`
--

CREATE TABLE `ket_surat` (
  `id_ket_surat` varchar(5) NOT NULL,
  `keterangan_surat` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ket_surat`
--

INSERT INTO `ket_surat` (`id_ket_surat`, `keterangan_surat`) VALUES
('B0', 'SURAT KELUAR INTERN'),
('B1', 'SURAT KELUAR EKSTERN'),
('SI', 'SURAT INSTRUKSI'),
('SK', 'SURAT KEPUTUSAN'),
('SP', 'SURAT PERINGATAN'),
('SPE', 'SURAT PENGUMUMAN'),
('STG', 'SURAT TEGURAN');

-- --------------------------------------------------------

--
-- Table structure for table `tb_disposisi`
--

CREATE TABLE `tb_disposisi` (
  `no_disposisi` varchar(16) NOT NULL,
  `no_agenda` varchar(15) NOT NULL,
  `no_surat` varchar(15) NOT NULL,
  `kepada` varchar(30) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `status_surat` enum('SB','B') NOT NULL,
  `tanggapan` enum('Teruskan','Tidak diteruskan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_disposisi`
--

INSERT INTO `tb_disposisi` (`no_disposisi`, `no_agenda`, `no_surat`, `kepada`, `keterangan`, `status_surat`, `tanggapan`) VALUES
('DSP-SM/0001', '0000000002', 'SRT-M/0002', 'Bos Besar', 'asdasd', '', 'Teruskan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `id` varchar(15) NOT NULL,
  `nama_depan` varchar(30) NOT NULL,
  `nama_belakang` varchar(30) NOT NULL,
  `j_kelamin` enum('L','P') NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL,
  `hak` varchar(13) NOT NULL,
  `foto_usr` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`id`, `nama_depan`, `nama_belakang`, `j_kelamin`, `username`, `password`, `hak`, `foto_usr`) VALUES
('PTGS0001', 'Tedy', 'Hidayat', 'L', 'admin', 'admin', 'Administrator', 'ARSPSRT_tedy.png'),
('PTGS0002', 'Gilang', 'Chandra syahputra', 'L', 'user', 'user', 'User', 'ARSPSRT_stock-illustration-43964724-flat-call-center-icon copy1.png'),
('PTGS0003', 'Dimas', 'Ramadhan', 'L', 'admin1', 'admin1', 'Administrator', 'ARSPSRT_ford escape.png'),
('PTGS0004', 'Albert', 'Einstein', 'L', 'user1', '123', 'User', 'ARSPSRT_avanza-23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_surat_keluar`
--

CREATE TABLE `tb_surat_keluar` (
  `no_agenda` varchar(15) NOT NULL,
  `id` varchar(15) NOT NULL,
  `jenis_surat` varchar(30) NOT NULL,
  `tanggal_kirim` varchar(15) NOT NULL,
  `no_surat` varchar(15) NOT NULL,
  `pengirim` varchar(30) NOT NULL,
  `perihal` varchar(30) NOT NULL,
  `file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_surat_keluar`
--

INSERT INTO `tb_surat_keluar` (`no_agenda`, `id`, `jenis_surat`, `tanggal_kirim`, `no_surat`, `pengirim`, `perihal`, `file`) VALUES
('0000000001', 'PTGS0001', 'B0', '2018-08-30', 'SRT-K/0001', 'Akbar', 'Pemanggilan kepala smk negeri ', 'ARSPSRT_Agama.docx'),
('0000000002', 'PTGS0001', 'SK', '2018-08-21', 'SRT-K/0002', 'Tedy Hidayat', 'Panggilan kerja', 'ARSPSRT_cut.docx');

-- --------------------------------------------------------

--
-- Table structure for table `tb_surat_masuk`
--

CREATE TABLE `tb_surat_masuk` (
  `no_agenda` varchar(15) NOT NULL,
  `id` varchar(15) NOT NULL,
  `jenis_surat` varchar(30) NOT NULL,
  `tanggal_kirim` varchar(15) NOT NULL,
  `tanggal_terima` varchar(15) NOT NULL,
  `no_surat` varchar(15) NOT NULL,
  `pengirim` varchar(30) NOT NULL,
  `perihal` varchar(30) NOT NULL,
  `file` varchar(100) NOT NULL,
  `status` enum('S','B') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_surat_masuk`
--

INSERT INTO `tb_surat_masuk` (`no_agenda`, `id`, `jenis_surat`, `tanggal_kirim`, `tanggal_terima`, `no_surat`, `pengirim`, `perihal`, `file`, `status`) VALUES
('0000000002', 'PTGS0001', 'SK', '2018-08-03', '2018-08-04', 'SRT-M/0002', 'Arif Fadhila SN', 'Pemecatan karyawan ', 'ARSPSRT_ppkn sidang.docx', 'S'),
('0000000003', 'PTGS0001', 'B1', '2018-08-14', '2018-08-29', 'SRT-M/0003', 'Akbar', 'Panggilan', 'ARSPSRT_1. KATA PENGANTAR.docx', 'B');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ket_surat`
--
ALTER TABLE `ket_surat`
  ADD PRIMARY KEY (`id_ket_surat`);

--
-- Indexes for table `tb_disposisi`
--
ALTER TABLE `tb_disposisi`
  ADD PRIMARY KEY (`no_disposisi`),
  ADD KEY `no_agenda` (`no_agenda`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_surat_masuk`
--
ALTER TABLE `tb_surat_masuk`
  ADD PRIMARY KEY (`no_agenda`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_disposisi`
--
ALTER TABLE `tb_disposisi`
  ADD CONSTRAINT `tb_disposisi_ibfk_1` FOREIGN KEY (`no_agenda`) REFERENCES `tb_surat_masuk` (`no_agenda`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
