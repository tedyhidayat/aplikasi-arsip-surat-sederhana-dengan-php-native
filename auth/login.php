<?php
@session_start();
include '../_config/config.php';

if(@$_SESSION['Administrator'] || @$_SESSION['User']){
    header("location: ../index.php");
}
else{
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman Login - Aplikasi Arsip Surat</title>
    <link rel="stylesheet" type="text/css" href="../_assets/css/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../_assets/css/style-login.css">
    <script src="../_assets/css/bootstrap/js/jquery.min.js"></script>
    <script src="../_assets/css/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<section class="login" id="login">
    <div class="col-sm-4 col-sm-offset-4 form-login wrapper">
        <div class="outter-form-login">
            <div class="row">
                <div class="col-sm-12 text-center judul">
                    <img src="../_assets/images/1.png" alt="User" class="img-circle" style="padding: 10px;">
                    <h4>Login</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 text-center content">
                    <form class="inner-login" action="" method="post">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-user"></i></div>
                                    <input class="form-control" type="text" name="username" placeholder="Username" autocomplete="off" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></div>
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="btn btn-primary btn-block btn-lg" type="submit" value="Masuk" name="masuk">
                            </div>
                            <div class="form-group text-center" style="padding-top: 10px;">
                              <!-- <font size="2">Belum punya akun ? <a href="#">Daftar</a></font> -->
                            </div>
                    </form>
                </div>
                <?php
                $username   = @$_POST['username'];
                $password   = @$_POST['password'];
                $login      = @$_POST['masuk'];

                if($login){
                    if($username == "" || $password == ""){
                        ?>
                            <script type="text/javascript">
                                alert("Username or password tidak boleh kosong !");
                                window.location.href ="login.php";
                            </script>
                        <?php
                    }
                    else{
                        $sql = mysqli_query($conn,"SELECT * FROM  tb_petugas WHERE username='$username' and password='$password'") or die (mysqli_error($conn));
                        $data_user = mysqli_fetch_array($sql);
                        $cek_login = mysqli_num_rows($sql);
                        if($cek_login >= 1){
                            if($data_user['hak'] == "User"){
                                @$_SESSION['User'] = $data_user['id'];
                                header("location:../user/index.php");
                            }
                            else if($data_user['hak'] == "Administrator"){
                                @$_SESSION['Administrator'] = $data_user['id'];
                                header("location:../index.php");
                            }
                        }
                        else{
                            ?>
                            <div class="col-sm-12">
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <strong>Login Gagal !</strong> Username atau Password salah.
                                </div>
                            </div>
                            <?php
                        }
                    }
                }

                ?>
            </div>
            <!--div class="row">
                <div class="col-sm-12 text-center">
                    <font style="font-size: 0.9em;">&copy 2018 - Tedy Hidayat</font>
                </div>
            </div -->
        </div>
    </div>
</section>
</body>
</html>

<?php
}
?>